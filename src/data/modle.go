package data

type Hotspot struct {
	Id        int `orm:"pk"`
	Name      string
	Latitude  float64
	Longitude float64
	Geohash   string
}
type Result struct {
	State bool
	Msg    string
	Data   interface{}
}

func ErrorResult(msg string) *Result {
	r := new(Result)
	r.State = false
	r.Msg = msg
	r.Data = nil
	return r
}
func SuccessResult(msg string) *Result {
	r := new(Result)
	r.State = true
	r.Msg = msg
	r.Data = nil
	return r
}
