package data

import (
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

func init() {
	orm.Debug = true
	orm.RegisterDriver("mysql", orm.DR_MySQL)
	orm.RegisterDataBase("default", "mysql", "root:adgjmptw@tcp(localhost:3306)/geohash?charset=utf8&loc=Asia%2FShanghai")
	orm.RegisterModel(new(Hotspot))
}
