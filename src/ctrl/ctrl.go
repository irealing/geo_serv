package ctrl

import (
	"data"
	"encoding/json"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/gansidui/geohash"
	"log"
)

type GeoCtrl struct {
	beego.Controller
}

func (this *GeoCtrl) Post() {
	log.Print("GeoCtrl->Post")
	strData := this.GetString(REQUEST_DATA, "")
	log.Println("GeoCtrl->Data", strData)
	if l := len(strData); l == 0 {
		this.Data["json"] = data.ErrorResult("数据异常")
	} else {
		this.Data["json"] = this.doLogic(&strData)
	}
	this.ServeJson()
}
func (this *GeoCtrl) doLogic(str *string) *data.Result {
	hs := new(data.Hotspot)
	err := json.Unmarshal([]byte(*str), hs)
	hs.Geohash, _ = geohash.Encode(hs.Latitude, hs.Longitude, 30)
	if err != nil {
		return data.ErrorResult(err.Error())
	}
	o := orm.NewOrm()
	_, err = o.Insert(hs)
	if err != nil {
		return data.ErrorResult(err.Error())
	}
	return data.SuccessResult("操作成功!")
}

type NearbyHotSpot struct {
	beego.Controller
}

func (this *NearbyHotSpot) Post() {
}
