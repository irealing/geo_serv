package router

import (
	"ctrl"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/geohash", &ctrl.GeoCtrl{})
	beego.Router("/nearby", &ctrl.NearbyCtrl{})
}
