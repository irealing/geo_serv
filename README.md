#GEOHASH测试项目服务器#
---

##开发环境
---
* 集成开发工具（IDE）:[eclipse](http://www.eclipse.org)
* 开发语言：[golang](http://www.golang.org)
* 应用框架 : [beego](http://github.com/astaxie/beego)

##数据交互
---
- 基于http协议

###服务器返回数据格式
    {"Status":true ,    //bool 状态
      "Msg":"操作成功",  //string
      "Data":{          //interface{}
      }
    }
###客户端发送数据
- 标准JSON字符串